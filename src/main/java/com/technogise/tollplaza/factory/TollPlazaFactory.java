/**
 * TollPlazaFactory class is used to get instance of toll plaza on the basis of vehicle specifications
 */
package com.technogise.tollplaza.factory;

import com.technogise.tollplaza.exceptions.InvalidDataException;
import com.technogise.tollplaza.model.TollInvoiceModel;
import com.technogise.tollplaza.model.VehicleModel;
import com.technogise.tollplaza.toll.DefaultToll;
import com.technogise.tollplaza.toll.FourWheelerToll;
import com.technogise.tollplaza.toll.ITollPlaza;
import com.technogise.tollplaza.toll.MultiAxleToll;
import com.technogise.tollplaza.toll.SixWheelerToll;
import com.technogise.tollplaza.toll.ThreeWheelerToll;
import com.technogise.tollplaza.toll.TwoWheelerToll;
import com.technogise.tollplaza.util.TollConstants;
import com.technogise.tollplaza.util.TollPlazaUtils;

/**
 * @author SJain
 * @version 1.0
 */
public class TollPlazaFactory {
	public ITollPlaza getTollPlaza(TollInvoiceModel tollInvoiceModel) throws InvalidDataException {
		int numberOfWheels = tollInvoiceModel.getVehicleModel().getNumberOfWheels();
		int numberOfAxle = tollInvoiceModel.getVehicleModel().getNumberOfAxle();
		String state = tollInvoiceModel.getState();
		VehicleModel vehicleModel = tollInvoiceModel.getVehicleModel();
		String isOnGovtDuty = vehicleModel.isOnGovtDuty().toLowerCase();

		if (!TollPlazaUtils.isValidData(tollInvoiceModel))
			throw new InvalidDataException(TollConstants.INVALID_DATA);

		if (TollPlazaUtils.isStateSame(vehicleModel, state) || isOnGovtDuty.equals(TollConstants.YES))
			return new DefaultToll(tollInvoiceModel);
		else if (numberOfWheels == 2)
			return new TwoWheelerToll(tollInvoiceModel);
		else if (numberOfWheels == 3)
			return new ThreeWheelerToll(tollInvoiceModel);
		else if (numberOfWheels == 4)
			return new FourWheelerToll(tollInvoiceModel);
		else if (numberOfWheels == 6 && numberOfAxle < 2)
			return new SixWheelerToll(tollInvoiceModel);
		else if (numberOfWheels >= 4 && numberOfAxle >= 2)
			return new MultiAxleToll(tollInvoiceModel);
		else
			throw new InvalidDataException(TollConstants.UNDEFINED_TOLL);
	}
}
